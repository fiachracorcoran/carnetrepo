//The root URL for the RESTful services
var rootURL = "http://localhost:8080/CarNet/rest/cars";

var car;

$(document).ready(function(){
	
	findAll();

	$('#btnSearch').click(function() {
		clearTables();
		search($('#searchKey').val());
		return false;
	});

	$('.list_cars_scroll').on('click','a', function() {
		findById($(this).data('id'));
	});
	
	$('#show_car').append('<img src="img/design/default_placeholder.jpg" alt="img/design/default_placeholder.jpg"/>')

	$('#types').val('all');
	$('#types').change(function(){
		if ($(this).val() == "all") {
			$('.scroll_cars').remove();
			rootURL = "http://localhost:8080/CarNet/rest/cars/all";
			findAll()
		} 
		else if($(this).val() == "new"){
			$('.scroll_cars').remove();
			rootURL = "http://localhost:8080/CarNet/rest/cars/new";
			findAll();
		}
		else if($(this).val() == "used"){
			$('.scroll_cars').remove();
			rootURL = "http://localhost:8080/CarNet/rest/cars/used";
			findAll();
		}
	});
});


function findAll() {
	console.log('find'+$('#types').val());
	$.ajax({
		type: 'GET',
		url: rootURL,
		dataType: "json", 
		success: renderList
	});
}

function findById(id) {
	$.ajax({
		type: 'GET',
		url: 'http://localhost:8080/CarNet/rest/cars/' + id,
		dataType: "json",
		success: function(data){
			console.log('findById success: ' + data.make);
			renderShowCarDiv(data);
		}
	});
}

function renderList(data) {
	var carList = data == null ? [] : (data instanceof Array ? data : [data]);
	$.each(carList, function(index, car) {
		$('.list_cars_scroll').append('<div class="scroll_cars"><a href="#" data-id='+ car.id + '><img src="img/cars/'+car.image+'" alt="img/design/default_placeholder.jpg"/></a><p><a href="#" data-id='+ car.id + '>'+car.make+' '+car.model+'</a><br>'+car.year+' - '+car.litre+' Litre<br>&euro;'+car.price+'</p></div>');
	});
}

function renderShowCarDiv(car) {
	$('#show_car').empty();
	$('#show_car').append('<img src="img/cars/'+car.image+'" alt="img/design/default_placeholder.jpg"/>'
						+'<div class="left"><h1>'+car.make+' '+car.model+'</h1>'
							+'<table class="car_details_table">'
									+'<tr><td>Year:</td><td>'+car.year+'</td></tr>'
									+'<tr><td>Engine:</td><td>'+car.litre+' litre</td></tr>'
									+'<tr><td>Mileage:</td><td>'+car.milage+' miles</td></tr>'
									+'<tr><td>Colour:</td><td>'+car.color+'</td></tr>'
									+'<tr><td>Price:</td><td>&euro;'+car.price+'</td></tr>'
							+'</table>'
						+'</div>'
					+'</div>');
}



