package com.ait.cars;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/cars")
@Stateless
@LocalBean
public class CarsWS {

	@EJB
	private CarDAO carDAO;
		
	@GET @Path("/all")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response findAllCars() {
		System.out.println("Get all cars");
		List<Car> cars=carDAO.getAllCars();
		return Response.status(200).entity(cars).build();
	}
	
	@GET @Path("/{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response findCarById(@PathParam("id") int id) {
		Car car = carDAO.getCarById(id);
		return Response.status(200).entity(car).build();
	}
	
	@GET @Path("/used")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response findAllUsedCars() {
		System.out.println("Get all used cars");
		List<Car> usedCars=carDAO.getAllUsedCars();
		return Response.status(200).entity(usedCars).build();
	}
	
	@GET @Path("/new")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response findAllNewCars() {
		System.out.println("Get all new cars");
		List<Car> newCars=carDAO.getAllNewCars();
		return Response.status(200).entity(newCars).build();
	}

}
