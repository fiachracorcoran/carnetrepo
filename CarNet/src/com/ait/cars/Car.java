package com.ait.cars;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
@Table(name="cars")
public class Car {

	@Id
	 @Column(name = "_id")
	private int id;

	    private String make;

	    private String model;

	    private String year;

	    private String color;
	    
	    private double litre;
	    
	    @Column(name = "mileage")
	    private String mileage;

	    private double price;
	    
	    private String image;
	    
	    private String seller;
	    
	    @Column(name = "cond")
	    private String condition;

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		
		public String getMake() {
			return make;
		}

		public void setMake(String make) {
			this.make = make;
		}

		public String getModel() {
			return model;
		}

		public void setModel(String model) {
			this.model = model;
		}
		
		public String getYear() {
			return year;
		}

		public void setYear(String year) {
			this.year = year;
		}
		
		public String getColor() {
			return color;
		}

		public void setColor(String color) {
			this.color = color;
		}

		public double getLitre() {
			return litre;
		}

		public void setLitre(double litre) {
			this.litre = litre;
		}

		public String getMilage() {
			return mileage;
		}

		public void setMilage(String mileage) {
			this.mileage = mileage;
		}

		public double getPrice() {
			return price;
		}

		public void setPrice(double price) {
			this.price = price;
		}
		
		public String getImage() {
			return image;
		}

		public void setImage(String image) {
			this.image = image;
		}

		public String getSeller() {
			return seller;
		}

		public void setSeller(String seller) {
			this.seller = seller;
		}
		
		public String getCondition() {
			return condition;
		}

		public void setCondition(String condition) {
			this.condition = condition;
		}
}