package com.ait.cars;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
@LocalBean
public class CarDAO {

    @PersistenceContext
    private EntityManager em;
    
    public List<Car> getAllCars() {
		Query query=em.createQuery("FROM Car c");
        return query.getResultList();
	}
    
    public Car getCarById(int id) {
		return em.find(Car.class, id);
	}

	public List<Car> getAllUsedCars() {
		Query query=em.createQuery("FROM Car c WHERE c.condition = :condition").setParameter("condition", "Used");
        return query.getResultList();
	}

	public List<Car> getAllNewCars() {
		Query query=em.createQuery("FROM Car c WHERE c.condition = :condition").setParameter("condition", "New");
        return query.getResultList();
	}
	
}
